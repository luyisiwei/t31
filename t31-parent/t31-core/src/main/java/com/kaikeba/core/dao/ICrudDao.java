package com.kaikeba.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kaikeba.core.po.BaseEntity;

import java.util.List;

public interface ICrudDao<T extends BaseEntity> extends BaseMapper<T>  {
    /**
     * 通过继承该接口，动态sql的查询实现
     */
    public List<T> selectByPage(T entity);
}
