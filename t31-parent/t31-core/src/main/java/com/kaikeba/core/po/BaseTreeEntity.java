package com.kaikeba.core.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = {"handler"})
public class BaseTreeEntity extends BaseEntity {
    /**
     * 培训字段
     */
    @TableField("order")
    private Integer order;
    /**
     * 父节点字段
     */
    @TableField("parent_id")
    private  Long parentId;
    /**
     * 节点字段
     */
    @TableField("title")
    private String title;
    /**
     * 是否展开
     */
    @TableField("expand")
    private Boolean expand;
}
