package com.kaikeba.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.kaikeba.core.po.BaseEntity;

import java.util.List;


public interface ICrudService<T extends BaseEntity> extends IService<T> {
    PageInfo<T> listPage(T entity, Integer pageNum, Integer pageSize);
    List<T> list(T entity);
    }
