package com.kaikeba.core.po;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ResponseBean {
    private Object model;
    private Boolean success;
    private String msg;

}
