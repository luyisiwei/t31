package com.kaikeba.core.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(value = {"handler"})
public abstract class BaseEntity implements Serializable {
    /**
     * 实体类的唯一标识
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
}

