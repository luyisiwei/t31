package com.kaikeba.core.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kaikeba.core.dao.ICrudDao;
import com.kaikeba.core.po.BaseEntity;

import java.util.List;
public class CrudServiceImp<T extends BaseEntity> extends ServiceImpl<ICrudDao<T> ,T> implements ICrudService<T> {
    @Override
    public PageInfo<T> listPage(T entity, Integer pageNum, Integer pageSize) {
        return PageHelper.startPage(pageNum,pageSize).doSelectPageInfo(()->{
            baseMapper.selectByPage(entity);
        });
    }

    @Override
    public List<T> list(T entity) {
        return getBaseMapper().selectList(Wrappers.emptyWrapper());
    }
}
