/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:3306
 Source Schema         : t31

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 03/11/2021 21:47:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carriage
-- ----------------------------
DROP TABLE IF EXISTS `carriage`;
CREATE TABLE `carriage`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number_id` bigint(20) NOT NULL COMMENT '车次ID',
  `carriage_no` int(20) NOT NULL COMMENT '车厢编号',
  `seate_type_id` bigint(20) NOT NULL COMMENT '座位类型',
  `count` int(20) NOT NULL COMMENT '座位数量',
  `price` double(10, 2) NOT NULL COMMENT '价格',
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carriage
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_date` datetime NOT NULL COMMENT '下单日期',
  `user_id` bigint(20) NOT NULL COMMENT '下单人',
  `train_date` datetime NOT NULL COMMENT '出发日期',
  `number_id` bigint(20) NOT NULL COMMENT '车次ID',
  `train_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列车号',
  `start_station` bigint(20) NOT NULL COMMENT '起始站',
  `end_station` bigint(20) NOT NULL COMMENT '终点站',
  `from_station` bigint(20) NOT NULL COMMENT '到达站',
  `start_time` datetime NOT NULL COMMENT '发车时间',
  `arrival_time` datetime NOT NULL COMMENT '到达时间',
  `duration` datetime NOT NULL COMMENT '期间',
  `from_no` datetime NULL DEFAULT NULL COMMENT '从',
  `no_to` datetime NULL DEFAULT NULL COMMENT '到',
  `seate_type_id` bigint(20) NOT NULL COMMENT '座位类型',
  `price` double(10, 2) NOT NULL COMMENT '票价',
  `passger_num` int(5) NOT NULL COMMENT '乘客人数',
  `total_number` double(10, 2) NOT NULL COMMENT '合计金额',
  `status` tinyint(1) NOT NULL COMMENT '订单状态',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for passager
-- ----------------------------
DROP TABLE IF EXISTS `passager`;
CREATE TABLE `passager`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `id_card` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '身份证号',
  `phone` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `status` tinyint(1) NOT NULL COMMENT '乘客认证状态',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of passager
-- ----------------------------

-- ----------------------------
-- Table structure for seat_type
-- ----------------------------
DROP TABLE IF EXISTS `seat_type`;
CREATE TABLE `seat_type`  (
  `id` tinyint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '座位等级',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of seat_type
-- ----------------------------

-- ----------------------------
-- Table structure for station
-- ----------------------------
DROP TABLE IF EXISTS `station`;
CREATE TABLE `station`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `station_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车站名称',
  `station_address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车站地址',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of station
-- ----------------------------

-- ----------------------------
-- Table structure for stopover_station
-- ----------------------------
DROP TABLE IF EXISTS `stopover_station`;
CREATE TABLE `stopover_station`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `station_id` bigint(20) NOT NULL COMMENT '经停车站名称',
  `number_id` bigint(20) NOT NULL COMMENT '车次ID',
  `num` int(20) NOT NULL COMMENT '编号',
  `train_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列车号',
  `arrive_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '到达类型',
  `arrive_time` datetime NOT NULL COMMENT '达到时间',
  `start_time` datetime NOT NULL COMMENT '出发时间',
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `duration` datetime NOT NULL COMMENT '期间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stopover_station
-- ----------------------------

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `passager_id` bigint(20) NOT NULL COMMENT '乘客ID',
  `passager_id_card` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '乘客身份证号',
  `train_number_id` bigint(20) NOT NULL COMMENT '车次ID',
  `train_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列车号',
  `start_station` bigint(255) NULL DEFAULT NULL COMMENT '起始站',
  `end_station` bigint(20) NOT NULL COMMENT '终点站',
  `from_station` bigint(20) NOT NULL COMMENT '出发站',
  `to_station` bigint(20) NOT NULL COMMENT '到达站',
  `start_time` datetime NOT NULL COMMENT '开车时间',
  `arrival_time` datetime NOT NULL COMMENT '到达时间',
  `duration` datetime NOT NULL COMMENT '期间时间',
  `from_no` int(11) NOT NULL COMMENT '从',
  `no_to` int(11) NOT NULL COMMENT '到',
  `seate_type_id` bigint(20) NOT NULL COMMENT '座位类型',
  `price` double(10, 2) NOT NULL COMMENT '票价',
  `status` tinyint(1) NOT NULL COMMENT '车票状态',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ticket
-- ----------------------------

-- ----------------------------
-- Table structure for train_number
-- ----------------------------
DROP TABLE IF EXISTS `train_number`;
CREATE TABLE `train_number`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车次',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型(G,C,T,K,L)',
  `carriage_count` int(20) NOT NULL COMMENT '车数',
  `start_station` bigint(20) NOT NULL COMMENT '起始站',
  `end_station` bigint(20) NOT NULL COMMENT '终点站',
  `start_time` datetime NOT NULL COMMENT '出发时间',
  `end_time` datetime NOT NULL COMMENT '到达时间',
  `arrive_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '到达类型',
  `duration` datetime NOT NULL COMMENT '期间',
  `run_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '运行类型',
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of train_number
-- ----------------------------

-- ----------------------------
-- Table structure for train_time
-- ----------------------------
DROP TABLE IF EXISTS `train_time`;
CREATE TABLE `train_time`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number_id` bigint(20) NOT NULL COMMENT '车次ID',
  `train_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列车号',
  `start_station` bigint(20) NOT NULL COMMENT '起始站',
  `end_station` bigint(20) NOT NULL COMMENT '终点站',
  `from_statiom` bigint(20) NOT NULL COMMENT '出发站',
  `to_station` bigint(20) NOT NULL COMMENT '到达站',
  `start_time` datetime NOT NULL COMMENT '出发时间',
  `arrival_time` datetime NOT NULL COMMENT '达到时间',
  `duration` datetime NOT NULL COMMENT '期间时间',
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of train_time
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '身份证姓名',
  `id_card` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '身份证号',
  `phone` char(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL COMMENT '实名认证状态',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
