package com.kaikeba.t31.admin.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaikeba.core.po.BaseTreeEntity;
import lombok.Data;

@Data
@TableName("admin-menu")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Menu extends BaseTreeEntity {
    @TableField("path")
    private String path;
    @TableField("name")
    private String name;
    @TableField("component")
    private String component;
    @TableField("hide_in_menu")
    private Boolean hideInMenu=false;
    @TableField("not_cache")
    private Boolean notCache;
    @TableField("icon")
    private String icon;
    @TableField(exist = false)
    private Long userId;
    @TableField(exist = false)
    private Long roleId;
    @TableField(exist = false)
    private Boolean selected;
    @TableField(exist = false)
    private Boolean checked;
}
