package com.kaikeba.t31.admin.service.Impl;

import com.kaikeba.core.service.CrudServiceImp;
import com.kaikeba.t31.admin.po.Menu;
import com.kaikeba.t31.admin.service.IMenuService;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImpl extends CrudServiceImp<Menu> implements IMenuService {
}
