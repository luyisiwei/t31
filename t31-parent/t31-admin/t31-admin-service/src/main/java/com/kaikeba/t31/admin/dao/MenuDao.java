package com.kaikeba.t31.admin.dao;

import com.kaikeba.core.dao.ICrudDao;
import com.kaikeba.t31.admin.po.Menu;

public interface MenuDao extends ICrudDao<Menu> {
}
