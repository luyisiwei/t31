package com.kaikeba.t31.admin.controller;

import com.kaikeba.core.controller.BaseController;
import com.kaikeba.t31.admin.po.Menu;
import com.kaikeba.t31.admin.service.IMenuService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController<IMenuService, Menu>  {
}
