package com.kaikeba.t31.admin.service;

import com.kaikeba.core.service.ICrudService;
import com.kaikeba.t31.admin.po.Menu;

public interface IMenuService extends ICrudService<Menu> {
}
