package com.kaikeba.t31.ticket.controller;

import com.kaikeba.core.controller.BaseController;
import com.kaikeba.core.po.ResponseBean;
import com.kaikeba.t31.ticket.service.IPassengerService;
import com.kaikeba.ticket.po.Passenger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/passenger")
public class PassengerController extends BaseController<IPassengerService, Passenger> {
    @Override
    public ResponseBean save(Passenger entity) {
        beforeSave(entity);
        ResponseBean bean = new ResponseBean();
        try{
            service.save(entity);
            bean.setMsg("新增成功").setSuccess(true).setModel(200);
        }catch (Throwable e){
            e.printStackTrace();
            bean.setMsg("新增失败").setSuccess(false).setModel(400);
        }

        return bean;
    }

    @Override
    public void beforeSave(Passenger entity) {
        entity.setStatus(1);
        entity.setUserId(1l);
    }
//    @Autowired
//    private IPassengerService userClient;
//    @Override
//    public Passenger edit(Long id) {
////        return super.edit(id);
//        Passenger client = service.getById(id);
//        return client;
//    }
//
//    @Override
//    public PageInfo<Passenger> listPage(Passenger entity, Integer page, Integer row) {
////        return super.listPage(entity, page, row);
//        PageInfo<Passenger> passengerPageInfo = service.listPage(entity, page, row);
//        return  passengerPageInfo;
//    }
//
//    @Override
//    public List<Passenger> list(Passenger entity) {
////        return super.list(entity);
//        List<Passenger> passengerList = service.list(entity);
//        return  passengerList;
//    }
//
//
//    @Override
//    public ResponseBean save(Passenger entity) {
//        ResponseBean rm = new ResponseBean();
//        try{
//            boolean save = service.save(entity);
//            rm.setModel(entity);
//        }catch (Exception e){
//            e.printStackTrace();
//            rm.setSuccess(false);
//            rm.setMsg("保存失败");
//        }
//        return rm;
//
////        return super.save(entity);
//    }
//
//    @Override
//    public ResponseBean delete(Long id) {
////        return super.delete(id);
//        ResponseBean rm = new ResponseBean();
//        try{
//            userClient.removeById(id);
//            rm.setModel(id);
//        }catch (Exception e){
//            e.printStackTrace();
//            rm.setSuccess(false);
//            rm.setMsg("删除失败");
//        }
//        return rm;
//    }

}
