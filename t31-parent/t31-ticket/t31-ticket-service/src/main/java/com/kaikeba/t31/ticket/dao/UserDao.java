package com.kaikeba.t31.ticket.dao;

import com.kaikeba.core.dao.ICrudDao;
import com.kaikeba.ticket.po.User;

public interface UserDao extends ICrudDao<User> {
}
