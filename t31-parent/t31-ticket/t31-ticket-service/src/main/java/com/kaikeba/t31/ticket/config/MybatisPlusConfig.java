package com.kaikeba.t31.ticket.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import com.github.pagehelper.PageInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * 分页插件
 */
@Configuration
@MapperScan("com.**.dao")
public class MybatisPlusConfig implements MetaObjectHandler {
    //开启join的优化，只针对于left join
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor().setCountSqlParser(new JsqlParserCountOptimize(true));
    }
    @Bean
    public PageInterceptor pageInterceptor(){
        return new PageInterceptor();
    }
    /**
     * 设置插入数据时取当前系统时间
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date=new Date();
        this.setFieldValByName("create_time",date,metaObject);
        this.setFieldValByName("update_time",date,metaObject);
    }

    /**
     * 设置更新数据时，取更新时间
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Date date=new Date();
        this.setFieldValByName("update_time",date,metaObject);
    }

}
