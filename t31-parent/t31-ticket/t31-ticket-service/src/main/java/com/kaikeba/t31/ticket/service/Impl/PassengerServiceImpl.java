package com.kaikeba.t31.ticket.service.Impl;

import com.kaikeba.core.service.CrudServiceImp;
import com.kaikeba.t31.ticket.service.IPassengerService;
import com.kaikeba.ticket.po.Passenger;
import org.springframework.stereotype.Service;

@Service
public class PassengerServiceImpl extends CrudServiceImp<Passenger> implements IPassengerService {
}
