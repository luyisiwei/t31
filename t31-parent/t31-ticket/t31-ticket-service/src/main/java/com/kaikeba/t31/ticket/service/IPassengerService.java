package com.kaikeba.t31.ticket.service;

import com.kaikeba.core.service.ICrudService;
import com.kaikeba.ticket.po.Passenger;


public interface IPassengerService extends ICrudService<Passenger> {
}
