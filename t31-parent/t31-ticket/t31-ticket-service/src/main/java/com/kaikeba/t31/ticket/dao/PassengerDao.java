package com.kaikeba.t31.ticket.dao;

import com.kaikeba.core.dao.ICrudDao;
import com.kaikeba.ticket.po.Passenger;

public interface PassengerDao extends ICrudDao<Passenger> {
}
