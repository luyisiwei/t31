package com.kaikeba.ticket.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.kaikeba.core.po.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@TableName("passenger")
public class Passenger extends BaseEntity {
    @TableField("name")
    private  String name;
    @TableField("user_id")
    private Long userId;
    @TableField("id_card")
    private String idCard;
    @TableField("phone")
    private String phone;
    @TableField("status")
    private Integer status;
    @TableField("type")
    private String type;
    @TableField("address")
    private String address;
//    @TableField(value = "create_time",fill = FieldFill.INSERT)
//    private Date createTime;
//    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
//    private Date updateTime;
}
