package com.kaikeba.ticket.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.kaikeba.core.po.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@TableName("user")
public class User extends BaseEntity {
    @TableField("username")
    private String username;
    @TableField("password")
    private String password;
    @TableField("name")
    private String name;
    @TableField("id_card")
    private String IdCard;
    @TableField("phone")
    private String phone;
    @TableField("status")
    private Integer status;
//    @TableField(value = "create_time",fill = FieldFill.INSERT)
//    private Date createTime;
//    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
//    private Date updateTime;
}
