import  instance from '@/libs/api/index'
import Qs from 'qs'
import {mapMutations} from 'vues'
//namespace 模块的名称
//entity 对应模块中管理的实体名称

export  const  baseList={
  data (){
    return{
      namespace: '',
      entityName:'',
      total: '',
      pageSize:10,
      rows:[]
    }
  },
  methods:{
     add(){
       let r=this.$store.state.app.tagNameList.find((item)=>{
         return item.name=='edit_${this.namespace}_${this.entity}'
       })
       if(!r){
         this.$router.push({
           name: 'edit_${this.namespace}_${this.entityName}'
         })
       }else {
         this.closeTag(r)
         r.query={ id:''}
         this.$router.push(r)
       }
     },
    remove(id,index){
      this.$Modal.confirm(
        {
          title:'确认删除',
          content:'确认要删除吗',
          onOk:(response)=>{
            instance.get('/${this.namespace}/${this.entityName}/delete'+id).
              then(response=>{
              this.$Message.info('删除成功')
              this.query()
            }).catch(
              error=>{
                console.log(error)}
            )
          }
        }
      )
     },
    //批量删除
    removeBatch() {
       if (this.$refs.selection.getselection().length>0){
         this.$Modal.confirm(
           {
             title:'确认删除',
             content:'确认要删除吗',
             onOk:(response)=>{
               let  params =new URLSearchParams()
               this.$refs.selection.getselection().forEach((o)=>{
                 params.append('id',o.id)
               })
               instance.post('/${this.namespace}/${this.entityName}/delete',params).
               then(response=>{
                 this.$Message.info('删除成功')
                 this.query()
               }).catch(
                 error=>{
                   console.log(error)}
               )
             }
           }
         )
       }else {
         this.$Message.info('选择删除的数据')
       }
    },
  //修改
    edit(){
       let r=$store.state.app.tagNaveList.fing((item)=>{
         return item.name=='edit_${this.namespace}_${this.entity}'
       })
      if (!r){
        this.$router.push({
          name: 'edit_${this.namespace}_${this.entity}',
          query:{id:id}
        })
      }else {
        this.closeTag(r)
        r.query={id:id}
        this.$router.push(r)
      }
    },
    //查询
    query(){
       instance.post('/${this.namespace}/${this.entity}/list-page',Qs.stringify(this.forEach(response=>{
         this.rows=response.data.list
         this.total=response.data.total
       }).catch(error=>{
         console.log(error)
         })
       ))
    },
    //分页
    changePage(index){
       this.formData.page=index
      this.query()
    },
    //设置梅特行数
    changePageSize(size) {
       this.formData.page=1
      this.formData.rows=size
      this.query()
    },
    mounted(){
       let arrays =this.$router.path.split('/')
      this.namespace=arrays[1]
      this.entityName=arrays[2]
      this.query()
    }

  }
    }
